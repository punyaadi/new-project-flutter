import 'package:datapenduduk/screen/input_new_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InputLogin extends StatefulWidget {
  const InputLogin({ Key? key }) : super(key: key);

  @override
  _InputLoginState createState() => _InputLoginState();
}

class _InputLoginState extends State<InputLogin> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

void login() {
    if (email.text == 'adicakep@gmail.com' && password.text == '123') {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => InputNewData(),
          ));
    } else {
      SnackBar tulisan = SnackBar(content: Text('email atau password salah!'));
      ScaffoldMessenger.of(context).showSnackBar(tulisan);
    }
  }

  @override
  Widget build(BuildContext context) {
    final InputBorder border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: BorderSide(
        width: 1,
        color: Theme.of(context).primaryColor,
      ));
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 20,
            ),
            Align(
              alignment: Alignment.center,
              child: Text('Sign In',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Align(
              alignment: Alignment.center,
              child: Text('Welcome Back!',
              style: TextStyle(fontWeight: FontWeight.w500),
              ),
            ),
            const SizedBox(
              height: 89,
            ),
            TextFormField(
              controller: email,
              decoration: InputDecoration(
                hintStyle: Theme.of(context).textTheme.bodyText1,
                filled: true,
                fillColor: Color(0xffC0D6D1),
                hintText: 'Email',
                border: border,
                focusedBorder: border.copyWith(
                  borderSide: BorderSide(
                    width: 2,
                    color: Theme.of(context).primaryColor,
                  )),
                enabledBorder: border),
            ),
            const SizedBox(
              height: 15,
              ),
            TextFormField(
              controller: password,
              decoration: InputDecoration(
                hintStyle: Theme.of(context).textTheme.bodyText1,
                filled: true,
                fillColor: Color(0xffC0D6D1),
                hintText: 'Password',
                border: border,
                suffixIcon: Icon(Icons.remove_red_eye_rounded),
                focusedBorder: border.copyWith(
                  borderSide: BorderSide(
                    width: 2,
                    color: Theme.of(context).primaryColor,
                  )),
                enabledBorder: border),
            ),
            const SizedBox(
              height: 60,
            ),
            ElevatedButton(
              onPressed: login,
              child: const Text('Sign In',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
              style: ButtonStyle(
                elevation: MaterialStateProperty.all(0),
              ),
              ),
              const SizedBox(
                height: 25,
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  'Forgot Your Password?',
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
              ),
          ],
        ),
      ),
    );
  }
}