import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';

class InputNewData extends StatefulWidget {
  const InputNewData({Key? key}) : super(key: key);

  @override
  _InputNewDataState createState() => _InputNewDataState();
}

class _InputNewDataState extends State<InputNewData> {
  TextEditingController tinggal = TextEditingController();

  int jumlahtinggal = 0;
  String jenisKelamin = 'laki-laki';
  String statusAlamat = 'alamat';
  String satuantinggal = 'tahun';
  void increment() {
    setState(() {
      jumlahtinggal++;
    });
    tinggal.text = jumlahtinggal.toString();
  }

  void decrement() {
    setState(() {
      jumlahtinggal--;
    });
    tinggal.text = jumlahtinggal.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Data Penduduk')),
      body: ListView(
        padding: const EdgeInsets.only(top: 20),
        children: [
          TextFieldLabel(label: "Namamu Siapa..", hint: "Namaku.."),
          const SizedBox(height: 10),
          TextFieldLabel(label: "NIK", hint: "cth: 1234xxxxx"),
          const SizedBox(
            height: 10,
          ),
          TextFieldLabel(label: 'Kewarganegaraan', hint: 'cth: Wakanda..'),
          TextFieldLabel(
              label: 'Suku Bangsa', hint: 'cth: Suku Frisian Flag..'),
          const SizedBox(
            height: 10,
          ),
          TextFieldLabel(
              label: 'Agama', hint: 'cth: Islam,Kristen,Tidak punya..'),
          const SizedBox(
            height: 10,
          ),
          Text(
            'Kelahiran',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          const SizedBox(
            height: 15,
          ),
          TextFieldLabel(
              label: 'Tempat Lahir', hint: 'cth: Yogyakarta,Jakarta..'),
          const SizedBox(
            height: 10,
          ),
          Text('Jenis Kelamin'),
          ListTile(
            title: Text('Laki-Laki'),
            leading: Radio<String>(
              fillColor: MaterialStateProperty.all(Color(0xff377765)),
              value: 'laki-laki',
              groupValue: jenisKelamin,
              onChanged: (value) {
                setState(() {
                  jenisKelamin = value!;
                });
              },
            ),
          ),
          ListTile(
            title: Text('Perempuan'),
            leading: Radio<String>(
              fillColor: MaterialStateProperty.all(Color(0xff377765)),
              value: 'perempuan',
              groupValue: jenisKelamin,
              onChanged: (value) {
                setState(() {
                  jenisKelamin = value!;
                });
              },
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            'Alamat',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          const SizedBox(
            height: 15,
          ),
          Text('Status Alamat'),
          ListTile(
            title: Text('Alamat Asli (Sesuai KTP)'),
            leading: Radio<String>(
              fillColor: MaterialStateProperty.all(Color(0xff377765)),
              value: 'alamat',
              groupValue: statusAlamat,
              onChanged: (value) {
                setState(() {
                  statusAlamat = value!;
                });
              },
            ),
          ),
          ListTile(
            title: Text('Alamat Domisili'),
            leading: Radio<String>(
              fillColor: MaterialStateProperty.all(Color(0xff377765)),
              value: 'domisili',
              groupValue: statusAlamat,
              onChanged: (value) {
                setState(() {
                  statusAlamat = value!;
                });
              },
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          TextFieldLabel(
              label: 'Jalan/Blok/Dusun', hint: 'cth: jl.jalan Yuk..'),
          const SizedBox(
            height: 10,
          ),
          Row(children: [
            Expanded(
                child: TextFieldLabel(label: 'Nomor', hint: 'cth: 12A,14C..')),
            const SizedBox(width: 16),
            Expanded(
                child: TextFieldLabel(label: 'RT', hint: 'cth: RT12,RT20..')),
            const SizedBox(
              width: 16,
            ),
            Expanded(
                child: TextFieldLabel(label: 'RW', hint: 'cth: RW05,RW47..')),
          ]),
          const SizedBox(
            height: 25,
          ),
          TextFieldLabel(
              label: 'Kelurahan/Desa',
              hint: 'cth: Patangpuluhan,Kebon Agung..'),
          const SizedBox(
            height: 10,
          ),
          TextFieldLabel(
              label: 'Kecamatan', hint: 'cth: Wirobrajan,Kubu Raya..'),
          const SizedBox(
            height: 10,
          ),
          TextFieldLabel(
              label: 'Kabupaten', hint: 'cth: Kota Yogyakarta,Pontianak'),
          const SizedBox(
            height: 10,
          ),
          TextFieldLabel(
              label: 'Provinsi', hint: 'cth: DI Yogyakarta,Kalimantan Barat..'),
          const SizedBox(
            height: 10,
          ),
          TextFieldLabel(label: 'Kode Pos', hint: 'cth: 12345'),
          const SizedBox(
            height: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Lama Tinggal'),
              const SizedBox(
                height: 12,
              )
            ],
          ),
          Row(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Color(0xffC0D6D1),
                    border: Border.all(
                        color: Theme.of(context).primaryColor, width: 1),
                    borderRadius: BorderRadius.circular(9)),
                child: Row(
                  children: [
                    IconButton(
                      onPressed: decrement,
                      icon: Icon(Icons.remove),
                    ),
                    SizedBox(
                        width: 100,
                        child: TextFormField(
                          onChanged: (value) {
                            setState(() {
                              jumlahtinggal = int.parse(value);
                            });
                          },
                          keyboardType: TextInputType.number,
                          controller: tinggal,
                          decoration: InputDecoration(
                              hintStyle: Theme.of(context).textTheme.bodyText1,
                              hintText: 'Lama Tinggal',
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none),
                        )),
                    IconButton(onPressed: increment, icon: Icon(Icons.add))
                  ],
                ),
              ),
              SizedBox(width: 20),
            ],
          )
        ],
      ),
    );
  }
}

class TextFieldLabel extends StatelessWidget {
  const TextFieldLabel({
    Key? key,
    required this.label,
    required this.hint,
  }) : super(key: key);

  final String label;
  final String hint;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Text(label,
              style:
                  const TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
          TextField(
            decoration: InputDecoration(
              filled: true,
              fillColor: const Color(0xffC0D6D1),
              hintText: hint,
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
            ),
          )
        ],
      ),
    );
  }
}
